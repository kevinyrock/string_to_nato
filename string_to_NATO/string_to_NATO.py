from phoneticAlphabet import phonetic

userInput = input("Please enter your string: ").lower().replace(" ", "")

for i in range(len(userInput)):
    letter = userInput[i]
    if i == len(userInput)-1:
        print(phonetic[letter], end=' ')
    else:
        print(phonetic[letter], end='-')
print('\n')
    
    

