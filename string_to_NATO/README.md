string_to_NATO
==============

Python Version: 3.3.3

A script to prompt the user for a string input, then output the NATO phonetic equivalent.
